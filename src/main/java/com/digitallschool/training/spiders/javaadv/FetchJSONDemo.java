/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.javaadv;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class FetchJSONDemo {

    public static void main(String[] args) throws Exception {
        HttpClient client = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder(
                URI.create("http://localhost:8080/first-web2/authorsJSON")).build();

        Instant starttime = Instant.now();

        CountDownLatch latch = new CountDownLatch(2);
        
        CompletableFuture.supplyAsync(() -> {
            try {
                HttpResponse response = client.send(request, BodyHandlers.ofString());
                return response.body();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }).thenAccept(e -> {
            System.out.println(e);
            latch.countDown();
        });

        CompletableFuture.supplyAsync(() -> {
            try {
                HttpResponse response = client.send(request, BodyHandlers.ofString());
                return response.body();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }).thenAccept(e -> {
            System.out.println(e);
            latch.countDown();
        });

        System.out.println("Awaiting results");
        latch.await();
        System.out.println(ChronoUnit.MILLIS.between(starttime, Instant.now()));

        /*try{
            Thread.sleep(14000);
        }catch(InterruptedException e){
            e.printStackTrace();
        }*/
    }

    public static void main2(String[] args) throws Exception {
        HttpClient client = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder(
                URI.create("http://localhost:8080/first-web2/authorsJSON")).build();

        ExecutorService executor = Executors.newFixedThreadPool(3);

        Instant starttime = Instant.now();

        Future<Object> future1 = executor.submit(() -> {
            HttpResponse response = client.send(request, BodyHandlers.ofString());
            return response.body();
        });

        Future<String> future2 = executor.submit(() -> {
            HttpResponse response = client.send(request, BodyHandlers.ofString());
            return response.body().toString();
        });

        System.out.println(future1.get());
        System.out.println(future2.get());

        System.out.println(ChronoUnit.MILLIS.between(starttime, Instant.now()));

        executor.shutdown();
    }

    public static void main1(String[] args) throws Exception {
        HttpClient client = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder(
                URI.create("http://localhost:8080/first-web2/authorsJSON")).build();

        Instant starttime = Instant.now();

        HttpResponse response = client.send(request, BodyHandlers.ofString());
        System.out.println(response.body());

        HttpResponse response2 = client.send(request, BodyHandlers.ofString());
        System.out.println(response2.body());

        System.out.println(ChronoUnit.MILLIS.between(starttime, Instant.now()));
    }
}
